const themeCheckbox = document.getElementById('theme');
const cssLink = document.getElementById('theme-css');

const DARK_CSS = './dark.css';
const LIGHT_CSS = './light.css';

themeCheckbox.addEventListener('click', (event) => {
    if (event.target.checked) {
        cssLink.setAttribute('href', DARK_CSS);
        localStorage.setItem('theme', 'dark');
    } else {
        cssLink.setAttribute('href', LIGHT_CSS);
        localStorage.setItem('theme', 'light');
    }
});

const lastTheme = localStorage.getItem('theme') || 'light';

if (lastTheme === 'light') {
    cssLink.setAttribute('href', LIGHT_CSS);
} else {
    cssLink.setAttribute('href', DARK_CSS);
    themeCheckbox.checked = true;
}
